const functions = require('firebase-functions');
const cors = require('cors')({origin: true});
const nodemailer = require('nodemailer');

const gmailEmail = functions.config().gmail.email;
const gmailPassword = functions.config().gmail.password;
const mailTransport = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: gmailEmail,
        pass: gmailPassword,
    },
});

//
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.helloWorld = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    response.status(200).json({message: "Hello from Firebase!"});
  });
});

exports.mail = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
      const data = {
        subject: request.body.payload.title,
        text: request.body.payload.message,
        to: request.body.email,
      };
      const mailOptions = {
          from: `Irregolari softair <irregolarisoftair@gmail.com>`,
          to: request.body.email,
          subject: request.body.payload.title,
          text: request.body.payload.message,
      };
      return mailTransport.sendMail(mailOptions)
          .then(() => response.status(200).json({message: 'OK'}))
          .catch(err => response.status(500).json({error: err}));
  })
});