// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  log: true,
  firebase: {
    apiKey: 'AIzaSyADmssMIZgTIgJR2R_NKyvr6TfgAf9tz1o',
    authDomain: 'soci-a10e4.firebaseapp.com',
    databaseURL: 'https://soci-a10e4.firebaseio.com',
    projectId: 'soci-a10e4',
    storageBucket: 'soci-a10e4.appspot.com',
    messagingSenderId: '73168395731'
  },
  functionsUrl: 'https://us-central1-soci-a10e4.cloudfunctions.net/',
  functions: {
    test: 'helloWorld',
    mail: 'mail',
  }
};
