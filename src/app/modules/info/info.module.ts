import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidenavInfoComponent } from './components/sidenav-info/sidenav-info.component';
import { MaterialModule } from '../material.module';
import { InfoFormComponent } from './components/info-form/info-form.component';
import { InfoService } from './services/info.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfoListComponent } from './components/info-list/info-list.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    SidenavInfoComponent,
    InfoFormComponent,
    InfoListComponent,
  ],
  exports: [
    SidenavInfoComponent,
  ],
  providers: [
    InfoService,
  ]
})
export class InfoModule { }
