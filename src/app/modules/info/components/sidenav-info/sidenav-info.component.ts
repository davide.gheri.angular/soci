import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { StaticService } from '../../../main/services/static.service';
import { AuthService } from '../../../main/services/auth.service';

@Component({
  selector: 'app-sidenav-info',
  templateUrl: './sidenav-info.component.html',
  styleUrls: ['./sidenav-info.component.scss']
})
export class SidenavInfoComponent implements OnInit {

  @Output() close: EventEmitter<any> = new EventEmitter<any>();

  open = false;
  canEditInfo: boolean;

  constructor(
    public text: StaticService,
    public auth: AuthService
  ) {
    this.auth.hasRole('admin').subscribe(bool => this.canEditInfo = bool);
  }

  ngOnInit() {
  }

  closeAll() {
    this.open = false;
    this.close.emit(true);
  }

}
