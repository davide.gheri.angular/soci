import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { InfoService } from '../../services/info.service';
import { Observable } from 'rxjs/Observable';
import { isArray } from 'util';

@Component({
  selector: 'app-info-list',
  templateUrl: './info-list.component.html',
  styleUrls: ['./info-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InfoListComponent implements OnInit {

  infos: Observable<any>;

  constructor(
    public infoService: InfoService,
  ) {
    this.infos = this.infoService.getInfo()
      .map(info => {
        const fInfo = [];
        fInfo.push(
          {label: 'Presidente', value: info.presidente},
          {label: 'Vicepresidente', value: info.vicepresidente},
          {label: 'Segretario', value: info.segretario}
        );
        info.consiglieri.forEach(c => fInfo.push({label: 'Consigliere', value: c}));
        return fInfo;
      });
  }

  ngOnInit() {
  }

}
