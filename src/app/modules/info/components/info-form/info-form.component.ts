import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InfoService } from '../../services/info.service';
import { AuthService } from '../../../main/services/auth.service';

@Component({
  selector: 'app-info-form',
  templateUrl: './info-form.component.html',
  styleUrls: ['./info-form.component.scss']
})
export class InfoFormComponent implements OnInit {

  infoForm: FormGroup;
  canEditInfo: boolean;

  constructor(
    private fb: FormBuilder,
    public infoService: InfoService,
    public auth: AuthService,
  ) {
    this.auth.hasRole('admin').subscribe(bool => this.canEditInfo = bool);
  }

  ngOnInit() {
    this.infoForm = this.fb.group({
      presidente: ['', Validators.required],
      vicepresidente: [''],
      segretario: [''],
      consiglieri: this.fb.array([])
    });

    this.infoService.getInfo().subscribe(info => {
      this.infoForm.reset();
      this.infoForm.patchValue(info);
      const consiglieriControl = this.infoForm.controls['consiglieri'] as any;
      consiglieriControl.controls = [];
      info.consiglieri.forEach(c => this.addConsigliere(c));
    });

  }

  addConsigliere(c = null): void {
    this.consiglieriArray.push(this.fb.group({name: c}));
  }

  get consiglieriArray(): FormArray {
    return this.infoForm.get('consiglieri') as FormArray;
  }

  saveInfo() {
    if (this.canEditInfo) {
      if (this.infoForm.valid) {
        const data = this.infoForm.value;
        data.consiglieri = data.consiglieri.filter(c => c.name && c.name.trim() !== '')
          .map(c => c.name);
        console.log(data.consiglieri)
        this.infoService.updateInfo(data);
      }
    }
  }

}
