import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { ProgressBarService } from '../../main/services/progress-bar.service';

@Injectable()
export class InfoService {
  info: Observable<any>;
  infoRef: AngularFireObject<any>;

  constructor(
    public db: AngularFireDatabase,
    public progressBar: ProgressBarService,
  ) {
    this.infoRef = this.db.object('info');
  }

  getInfo() {
    this.info = this.infoRef.valueChanges();
    return this.info;
  }

  updateInfo(data): Promise<any> {
    this.progressBar.visible = true;
    return this.infoRef.set(data)
      .then(res => {
        this.progressBar.reset();
        return res;
      });
  }

}
