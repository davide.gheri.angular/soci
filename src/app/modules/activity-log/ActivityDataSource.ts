import { DataSource } from '@angular/cdk/collections';
import { CollectionViewer } from '@angular/cdk/typings/collections';
import { Observable } from 'rxjs/Observable';
import { Activity } from './models';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


export class ActivityDataSource implements DataSource<Activity> {

  data: BehaviorSubject<any> = new BehaviorSubject<any>([]);

  constructor(private _data: any) {}

  connect(collectionViewer: CollectionViewer): Observable<any[]> {
    this._data.subscribe((logs: Activity[]) => {
      const rows = [];
      logs.forEach(log => rows.push(log, {detailRow: true, log}));
      this.data.next(rows);
    });
    return this.data;
  }

  disconnect(collectionViewer: CollectionViewer): void {

  }
}
