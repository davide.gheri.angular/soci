import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, SnapshotAction } from 'angularfire2/database';
import { Activity } from '../models/activity';
import { AuthService } from '../../main/services/auth.service';
import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';
import { CommonService } from '../../shared/services/common.service';
import { environment } from '../../../../environments/environment';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class ActivitiesService extends CommonService {
  logRef: AngularFireList<Activity>;
  logs: Observable<Activity[]>;

  constructor(
    public db: AngularFireDatabase,
    public auth: AuthService,
  ) {
    super();
    this.logRef = this.db.list('activity_log');
  }

  log(changes, action, object = null, itemId = null): any {
    const data: Activity = {
      changes, action, object, itemId,
      user: this.auth.user,
      createdAt: firebase.database.ServerValue.TIMESTAMP,
    };
    if (environment.log) {
      return this.logRef.push(data);
    }
    return new Promise(res => res());
  }

  getlogs() {
    // this.logs = this.db.list('activity_log', ref => ref.orderByChild('createdAt').limitToLast(this.currentPerPage)).snapshotChanges()
    this.logs = this.logRef.snapshotChanges()
      .map((actions: SnapshotAction[]) => {
        return actions.map((action: SnapshotAction) => this.fromActionToModel<Activity>(action)).reverse();
      });
    return this.logs;
  }

}
