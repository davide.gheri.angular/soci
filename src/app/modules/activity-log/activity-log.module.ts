import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { SharedModule } from '../shared/shared.module';
import { ActivitiesComponent } from './components';
import { ActivitiesService } from './services/activities.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule
  ],
  declarations: [
    ActivitiesComponent,
  ],
  exports: [
    ActivitiesComponent,
  ],
  providers: [
    ActivitiesService,
  ]
})
export class ActivityLogModule { }
