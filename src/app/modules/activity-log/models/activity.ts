import { User } from '../../main/services/auth.service';

export class Activity {
  id?: string;
  changes: any;
  action: string;
  user: User;
  itemId?: string;
  object?: string;
  createdAt: any;
}
