import { Component, OnInit } from '@angular/core';
import { ActivitiesService } from '../../services/activities.service';
import { ActivityDataSource } from '../../ActivityDataSource';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.scss'],
  animations: [
    trigger('detailExpanded', [
      state('collapsed', style({height: '0px', minHeight: '0', visibility: 'hidden'})),
      state('expanded', style({height: '*', visibility: 'visible'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])
  ]
})
export class ActivitiesComponent implements OnInit {

  columns = ['data', 'user', 'target', 'action'];
  dataSource: ActivityDataSource;

  expandedElement: any;
  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');

  constructor(
    public logs: ActivitiesService
  ) {
  }

  ngOnInit() {
    this.dataSource = new ActivityDataSource(this.logs.getlogs());
  }

  expandRow(row) {
    if (this.expandedElement === row.id) {
      this.expandedElement = null;
    } else {
      this.expandedElement = row.id;
    }
  }

}
