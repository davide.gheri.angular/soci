import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule, MatListModule, MatSidenavModule, MatTabsModule,
  MatToolbarModule, MatTableModule, MatSortModule, MatProgressBarModule, MatProgressSpinnerModule, MatCardModule,
  MatExpansionModule,
  MatDatepickerModule, MatDateFormats, MatSelectModule, MatAutocompleteModule, MatDialogModule, MatRadioModule,
  MatCheckboxModule, MatPaginatorModule, MatMenuModule,
} from '@angular/material';
import { MAT_MOMENT_DATE_FORMATS, MatMomentDateModule, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

const CUSTOM_MOMENT_DATE_FORMATS: MatDateFormats = Object.assign(MAT_MOMENT_DATE_FORMATS, {
  display: Object.assign(MAT_MOMENT_DATE_FORMATS.display, {dateInput: 'L'}),
  parse: Object.assign(MAT_MOMENT_DATE_FORMATS.parse, {dateInput: 'L'})
});

@NgModule({
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatToolbarModule,
    MatSidenavModule,
    MatTabsModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatSortModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatDialogModule,
    MatPaginatorModule,
    MatMenuModule,
  ],
  exports: [
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatToolbarModule,
    MatSidenavModule,
    MatTabsModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatSortModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatDialogModule,
    MatPaginatorModule,
    MatMenuModule,
  ],
  declarations: [],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: CUSTOM_MOMENT_DATE_FORMATS},
    {provide: MAT_DATE_LOCALE, useValue: 'it-IT'}
  ]
})
export class MaterialModule { }
