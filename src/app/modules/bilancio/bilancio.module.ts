import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';
import { BilanciComponent, BilancioListComponent, BilancioRowEditComponent, NewBilancioComponent } from './components';
import { BilanciService } from './services/bilanci.service';
import { YearGuard } from './guards/year.guard';
import { ActivityLogModule } from '../activity-log/activity-log.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ActivityLogModule,
  ],
  exports: [
    BilancioListComponent,
    BilanciComponent,
    BilancioRowEditComponent,
    NewBilancioComponent,
  ],
  declarations: [
    BilancioListComponent,
    BilanciComponent,
    BilancioRowEditComponent,
    NewBilancioComponent,
  ],
  providers: [
    YearGuard,
    BilanciService,
  ]
})
export class BilancioModule { }
