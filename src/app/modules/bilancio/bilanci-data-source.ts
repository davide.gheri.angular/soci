import { DataSource } from '@angular/cdk/collections';
import { CollectionViewer } from '@angular/cdk/typings/collections';
import { Observable } from 'rxjs/Observable';
import { Bilancio } from './models';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


export class BilanciDataSource implements DataSource<Bilancio> {

  data: BehaviorSubject<any> = new BehaviorSubject<any>([]);

  constructor(private _data: any) {}

  connect(collectionViewer: CollectionViewer): Observable<any[]> {
    this._data.subscribe((bilanci: Bilancio[]) => {
      const rows = [];
      bilanci.forEach(bilancio => rows.push(bilancio, {detailRow: true, bilancio}));
      this.data.next(rows);
    });
    return this.data;
  }

  disconnect(collectionViewer: CollectionViewer): void {

  }
}
