import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Bilancio, BILANCIO_DATES } from '../../models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BilanciService } from '../../services/bilanci.service';
import * as moment from 'moment';
import { MatDialog } from '@angular/material';
import { DialogConfirmComponent } from '../../../shared/components/dialog-confirm/dialog-confirm.component';
import { MediaService } from '../../../shared/services/media.service';

@Component({
  selector: 'app-bilancio-row-edit',
  templateUrl: './bilancio-row-edit.component.html',
  styleUrls: ['./bilancio-row-edit.component.scss']
})
export class BilancioRowEditComponent implements OnInit, OnChanges {

  @Input() bilancio: Bilancio;
  @Input() year: number | string;
  bilancioForm: FormGroup;

  oldPartialTotal: {entrata: string | number, uscita: string | number};

  constructor(
    public fb: FormBuilder,
    public bilanciService: BilanciService,
    public dialog: MatDialog,
    public mediaService: MediaService,
  ) {
    this.bilancioForm = this.fb.group({
      data_inserimento: ['', Validators.required],
      effettuato_da: ['', Validators.required],
      entrata: [0, Validators.required],
      uscita: [0, Validators.required],
      causale: ['', Validators.required],
      controllo: [false]
    });
  }

  ngOnInit() {
    this.patchFormValue(this.bilancio);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['bilancio']) {
      const bilancio = <Bilancio>changes['bilancio'].currentValue;
      this.patchFormValue(bilancio);
      this.oldPartialTotal = {entrata: bilancio.entrata, uscita: bilancio.uscita};
    }
  }

  patchFormValue(data) {
    // Prevent undesired two way data-binding
    data = Object.assign({}, data);
    this.bilancioForm.patchValue(Object.keys(data).reduce((obj, field) => {
      if (BILANCIO_DATES.includes(field)) {
        obj[field] = moment(data[field], 'DD-MM-YYYY');
      }
      return obj;
    }, data));
  }

  saveBilancioChanges() {
    if (this.bilancioForm.valid) {
      const data = Object.keys(this.bilancioForm.value).reduce((obj, field) => {
        if (obj[field] && obj[field]._isAMomentObject) {
          obj[field] = obj[field].format('DD/MM/YYYY');
        }
        return obj;
      }, this.bilancioForm.value);
      this.bilanciService.updateBilancio(data, this.year, this.bilancio.id, this.oldPartialTotal);
    } else {
      Object.keys(this.bilancioForm.controls).forEach(ctrl => {
        this.bilancioForm.get(ctrl).markAsTouched();
        this.bilancioForm.get(ctrl).updateValueAndValidity();
      });
    }
  }

  openConfirmDialog() {
    this.dialog.open(DialogConfirmComponent, {
      data: {
        title: 'Vuoi davvero eliminare il bilancio del {data_inserimento}?',
        item: this.bilancio
      }
    }).afterClosed().subscribe(bool =>
      (bool && this.bilanciService.deleteBilancio(this.year, this.bilancio.id, this.bilancio))
    );
  }

}
