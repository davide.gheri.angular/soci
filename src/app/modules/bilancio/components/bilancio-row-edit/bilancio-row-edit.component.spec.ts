import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BilancioRowEditComponent } from './bilancio-row-edit.component';

describe('BilancioRowEditComponent', () => {
  let component: BilancioRowEditComponent;
  let fixture: ComponentFixture<BilancioRowEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BilancioRowEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BilancioRowEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
