import { Component, OnInit, ViewChild } from '@angular/core';
import { BilanciService } from '../../services/bilanci.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Bilancio } from '../../models';
import { BilanciDataSource } from '../../bilanci-data-source';
import { MatSort, Sort } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { combineLatest } from 'rxjs/observable/combineLatest';
import * as moment from 'moment';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { AuthService } from '../../../main/services/auth.service';

@Component({
  selector: 'app-bilanci',
  templateUrl: './bilanci.component.html',
  styleUrls: ['./bilanci.component.scss'],
  animations: [
    trigger('detailExpanded', [
      state('collapsed', style({height: '0px', minHeight: '0', visibility: 'hidden'})),
      state('expanded', style({height: '*', visibility: 'visible'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])
  ]
})
export class BilanciComponent implements OnInit {
  columns = ['data_inserimento', 'effettuato_da', 'entrata', 'uscita', 'causale'];
  dataSource: BilanciDataSource;

  bilanci$: Observable<Bilancio[]>;
  sort$: BehaviorSubject<Sort>;
  bilanci: Observable<Bilancio[]>;
  total: Observable<number>;
  year: string | number;

  @ViewChild(MatSort) sort: MatSort;

  canEditBilanci = false;

  expandedElement: any;
  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    public auth: AuthService,
    public bilanciService: BilanciService,
  ) {
    this.auth.hasRole('admin').subscribe(bool => this.canEditBilanci = bool);
    this.total = this.bilanciService.getTotal();
  }

  ngOnInit() {
    this.sort$ = new BehaviorSubject<Sort>({active: 'data_inserimento', direction: 'desc'});
    this.route.params.subscribe(params => {
      if (!params.year) {
        this.bilanciService.years.take(1).subscribe(years => {
          this.router.navigate(['/bilancio/' + years[0].id]);
        });
      } else {
        this.year = params.year;
        this.bilanci$ = this.bilanciService.getBilanci(params.year);
        this.bilanci = combineLatest(this.bilanci$, this.sort$, (bilanci: Bilancio[], sort: Sort) => {
          const filtered = bilanci;
          if (sort) {
            filtered.sort((a, b) => {
              const aDate = moment(a.data_inserimento, 'DD/MM/YYYY');
              const bDate = moment(b.data_inserimento, 'DD/MM/YYYY');
              if (aDate.isAfter(bDate)) {
                return sort.direction === 'desc' ? 1 : -1;
              }
              if (bDate.isAfter(aDate)) {
                return sort.direction === 'desc' ? -1 : 1;
              }
              return 0;
            });
          }
          return filtered;
        });
        this.dataSource = new BilanciDataSource(this.bilanci);
      }
    });
    this.sort.sortChange.subscribe((sort: Sort) => {
      this.sort$.next(sort);
    });
  }

  expandRow(row) {
    if (this.canEditBilanci) {
      if (this.expandedElement === row.id) {
        this.expandedElement = null;
      } else {
        this.expandedElement = row.id;
      }
    } else {
      this.expandedElement = null;
    }
  }

}
