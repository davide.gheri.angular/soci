import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BilanciComponent } from './bilanci.component';

describe('BilanciComponent', () => {
  let component: BilanciComponent;
  let fixture: ComponentFixture<BilanciComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BilanciComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BilanciComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
