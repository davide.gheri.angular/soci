import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BilancioListComponent } from './bilancio-list.component';

describe('BilancioListComponent', () => {
  let component: BilancioListComponent;
  let fixture: ComponentFixture<BilancioListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BilancioListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BilancioListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
