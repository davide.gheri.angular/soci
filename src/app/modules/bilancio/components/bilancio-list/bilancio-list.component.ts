import { Component, OnInit } from '@angular/core';
import { Year } from '../../models';
import { Observable } from 'rxjs/Observable';
import { BilanciService } from '../../services/bilanci.service';

@Component({
  selector: 'app-bilancio-list',
  templateUrl: './bilancio-list.component.html',
  styleUrls: ['./bilancio-list.component.scss']
})
export class BilancioListComponent implements OnInit {

  years: Observable<Year[]>;

  constructor(
    public bilanciService: BilanciService,
  ) { }

  ngOnInit() {
    this.years = this.bilanciService.years;
  }

}
