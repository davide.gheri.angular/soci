import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BilanciService } from '../../services/bilanci.service';
import { Moment } from 'moment';
import { Router } from '@angular/router';
import { MediaService } from '../../../shared/services/media.service';

@Component({
  selector: 'app-new-bilancio',
  templateUrl: './new-bilancio.component.html',
  styleUrls: ['./new-bilancio.component.scss']
})
export class NewBilancioComponent implements OnInit {

  currentYear = moment().format('YYYY');
  bilancioForm: FormGroup;

  dateFilter = (d: Moment): boolean => {
    return d.isAfter(moment(this.currentYear, 'YYYY'));
  }

  constructor(
    public bilanciService: BilanciService,
    public fb: FormBuilder,
    public router: Router,
    public mediaService: MediaService,
  ) { }

  ngOnInit() {
    this.bilancioForm = this.fb.group({
      data_inserimento: [moment(), Validators.required],
      effettuato_da: ['', Validators.required],
      entrata: [0, Validators.required],
      uscita: [0, Validators.required],
      causale: ['', Validators.required],
      controllo: [0, Validators.required],
    });
  }

  createBilancio() {
    if (this.bilancioForm.valid) {
      const data = Object.keys(this.bilancioForm.value).reduce((obj, field) => {
        if (obj[field] && obj[field]._isAMomentObject) {
          obj[field] = obj[field].format('DD/MM/YYYY');
        }
        return obj;
      }, this.bilancioForm.value);
      this.bilanciService.createBilancio(this.currentYear, data)
        .then(res => this.router.navigate(['/bilancio/' + this.currentYear]))
    } else {
      Object.keys(this.bilancioForm.controls).forEach(ctrl => {
        this.bilancioForm.get(ctrl).markAsTouched();
        this.bilancioForm.get(ctrl).updateValueAndValidity();
      });
    }
  }

}
