import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewBilancioComponent } from './new-bilancio.component';

describe('NewBilancioComponent', () => {
  let component: NewBilancioComponent;
  let fixture: ComponentFixture<NewBilancioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewBilancioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBilancioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
