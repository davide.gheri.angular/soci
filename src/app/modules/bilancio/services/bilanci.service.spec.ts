import { TestBed, inject } from '@angular/core/testing';

import { BilanciService } from './bilanci.service';

describe('BilanciService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BilanciService]
    });
  });

  it('should be created', inject([BilanciService], (service: BilanciService) => {
    expect(service).toBeTruthy();
  }));
});
