import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, SnapshotAction } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { Bilancio, Year } from '../models';
import { CommonService } from '../../shared/services/common.service';
import * as moment from 'moment';
import { ProgressBarService } from '../../main/services/progress-bar.service';
import { OverlayService } from '../../shared/services/overlay.service';
import { AngularFireObject } from 'angularfire2/database/interfaces';
import { ActivitiesService } from '../../activity-log/services/activities.service';
import { Activity } from '../../activity-log/models/activity';

interface PartialTotal {
  entrata: string | number;
  uscita: string | number;
}


@Injectable()
export class BilanciService extends CommonService {
  years: Observable<Year[]>;
  bilanci: Observable<Bilancio[]>;
  total: Observable<any>;
  _total: number;

  totalRef: AngularFireObject<number>;

  constructor(
    public db: AngularFireDatabase,
    public progressBar: ProgressBarService,
    public overlayService: OverlayService,
    public activityLog: ActivitiesService,
  ) {
    super();
    this.totalRef = this.db.object('bilancio_total');

    this.years = this.db.list<any>('bilancio', (ref) => ref.orderByChild('id')).snapshotChanges()
      .map((actions: SnapshotAction[]) => {
        return actions.map((action: SnapshotAction) => {
          const id = action.key;
          return {id} as Year;
        });
      })
      .map((years: Year[]) => {
        return years.sort((a, b) => a.id > b.id ? -1 : 1);
      });
  }

  getTotal() {
    this.total = this.totalRef.valueChanges();
    this.total.subscribe(total => {
      this._total = +total;
    });
    return this.total;
  }

  getBilanci(year): Observable<Bilancio[]> {
    this.bilanci = this.db.list('bilancio/' + year).snapshotChanges()
      .map((actions: SnapshotAction[]) => {
        return actions.map((action: SnapshotAction) => this.fromActionToModel<Bilancio>(action))
          .sort((a, b) => {
            const aDate = moment(a.data_inserimento, 'DD/MM/YYYY');
            const bDate = moment(b.data_inserimento, 'DD/MM/YYYY');
            return aDate.isAfter(bDate) ? 1 : -1;
          });
      });
    return this.bilanci;
  }

  updateBilancio(data, year, id, oldData: PartialTotal): Promise<any> {
    const ref = this.db.object(`bilancio/${year}/${id}`);
    this.progressBar.visible = true;
    return ref.update(data)
      .then(res => {
        this.activityLog.log(data, 'update', 'bilancio', id);
        this.updateTotal({entrata: data.entrata, uscita: data.uscita} as PartialTotal, oldData);
        this.progressBar.reset();
        return res;
      });
  }

  deleteBilancio(year, id, data): Promise<any> {
    const ref = this.db.object(`bilancio/${year}/${id}`);
    this.progressBar.visible = true;
    this.overlayService.visible.next(true);
    return ref.remove()
      .then(res => {
        this.progressBar.reset();
        this.activityLog.log(data, 'delete', id);
        this.removeToTotal({entrata: data.entrata, uscita: data.uscita} as PartialTotal);
        this.overlayService.visible.next(false);
        return res;
      });
  }

  createBilancio(year, data): any {
    this.progressBar.visible = true;
    return this.db.list(`bilancio/${year}`).push(data)
      .then(res => {
        this.activityLog.log(data, 'create', 'bilancio', res.key);
        this.addToTotal({entrata: data.entrata, uscita: data.uscita} as PartialTotal);
        this.progressBar.reset();
        return res.key;
      });
  }

  updateTotal(data: PartialTotal, oldData: PartialTotal): Promise<any> {
    const entrata = +data.entrata - +oldData.entrata;
    const uscita = -(+data.uscita - +oldData.uscita);
    let newTotal = this._total;
    newTotal += entrata;
    newTotal += uscita;
    return this.totalRef.set(newTotal);
  }

  addToTotal(data: PartialTotal): Promise<any> {
    let newTotal = this._total;
    newTotal += +data.entrata;
    newTotal -= +data.uscita;
    return this.totalRef.set(newTotal);
  }

  removeToTotal(data: PartialTotal): Promise<any> {
    let newTotal = this._total;
    newTotal -= +data.entrata;
    newTotal += +data.uscita;
    return this.totalRef.set(newTotal);
  }

}
