
export const BILANCIO_DATES = ['data_inserimento'];

export class Bilancio {
  id?: string;
  causale?: string;
  controllo?: string | number | boolean;
  data_inserimento?: string;
  entrata: number;
  uscita: number;
  effettuato_da: string;
}
