import { TestBed, async, inject } from '@angular/core/testing';

import { YearGuard } from './year.guard';

describe('YearGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [YearGuard]
    });
  });

  it('should ...', inject([YearGuard], (guard: YearGuard) => {
    expect(guard).toBeTruthy();
  }));
});
