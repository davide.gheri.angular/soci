import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent, MainComponent } from './main/components';
import { AuthGuard } from './main/guards/auth.guard';
import { SociComponent, SocioComponent, SocioShowComponent, SocioAddressComponent } from './soci/components';
import { RoleGuard } from './main/guards/role.guard';
import { BilanciComponent, NewBilancioComponent } from './bilancio/components';
import { ActivitiesComponent } from './activity-log/components';

export const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: '', component: MainComponent, canActivate: [AuthGuard],
    children: [
      {path: '', redirectTo: 'soci', pathMatch: 'full', data: {animation: 'soci'}},
      {path: 'soci/create/indirizzo', redirectTo: 'soci/create', pathMatch: 'full'},
      {path: 'soci/:id', component: SocioComponent,
        canActivate: [RoleGuard],
        data: {animation: 'socio', roles: ['admin']},
        children: [
          {path: '', component: SocioShowComponent, data: {animation: 'form'}},
          {path: 'indirizzo', component: SocioAddressComponent, data: {animation: 'address'}},
        ]},
      {path: 'soci', component: SociComponent},
      {path: 'bilancio/create', component: NewBilancioComponent},
      {path: 'bilancio/:year', component: BilanciComponent},
      {path: 'bilancio', component: BilanciComponent},
      {path: 'logs', component: ActivitiesComponent},
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  declarations: []
})
export class RouteModule { }
