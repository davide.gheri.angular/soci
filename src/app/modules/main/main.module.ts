import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LoginComponent, MainComponent, NavbarComponent } from './components';
import { AuthGuard } from './guards/auth.guard';
import { StaticService } from './services/static.service';
import { MaterialModule } from '../material.module';
import { AuthService } from './services/auth.service';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { InfoModule } from '../info/info.module';
import { ImportModule } from '../import/import.module';
import { SociModule } from '../soci/soci.module';
import { ProgressBarService } from './services/progress-bar.service';
import { SharedModule } from '../shared/shared.module';
import { OverlayComponent } from './components/overlay/overlay.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RoleGuard } from './guards/role.guard';
import { BilancioModule } from '../bilancio/bilancio.module';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    SharedModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule,
    InfoModule,
    ImportModule,
    SociModule,
    BilancioModule,
  ],
  declarations: [
    LoginComponent,
    MainComponent,
    NavbarComponent,
    SidenavComponent,
    OverlayComponent,
  ],
  providers: [
    AuthGuard,
    RoleGuard,
    StaticService,
    AuthService,
    ProgressBarService,
  ]
})
export class MainModule { }
