import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import { b64DecodeUnicode } from '../services/auth.service';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/take';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(
    private afAuth: AngularFireAuth,
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const rolesToCheck = next.data['roles'] as Array<string>;
    return this.afAuth.authState
      .map((authInfo: any) => {
        const { roles } = JSON.parse(b64DecodeUnicode(authInfo.qa.split('.')[1]));
        if (roles) {
          const rolesTrue = Object.keys(roles).reduce((array, role) => {
            if (roles[role] && roles[role] === true) {
              array.push(role);
            }
            return array;
          }, []);
          return rolesToCheck.some(role => rolesTrue.includes(role));
        }
        return false;
      })
      .take(1)
      .do(can => {
        if (!can) {
          this.router.navigate(['/login']);
        }
      });
  }
}
