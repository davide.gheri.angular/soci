import { animate, query, style, transition, trigger } from '@angular/animations';


export const routerTransition = trigger('routerTransition', [
  transition('* <=> *', [
    query(':enter', style({
      position: 'absolute',
      width: 'calc(100% - 60px)',
      // transform: 'translateX(-120%)'
      transform: 'scale(0)',
      'transform-origin': '50% 300%',
    }), {optional: true}),
    query(':leave', animate('400ms ease', style({
      position: 'absolute',
      width: 'calc(100% - 60px)',
      // transform: 'translateX(100%)'
      transform: 'translateY(100vh)',
      'transform-origin': '50% 300%',
    })), {optional: true}),
    query(':enter', animate('400ms ease', style({
      opacity: 1,
      // transform: 'translateX(0%)'
      transform: 'scale(1)',
      'transform-origin': '50% 300%',
    })), {optional: true})
  ])
]);;
