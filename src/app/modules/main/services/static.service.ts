import { Injectable } from '@angular/core';

@Injectable()
export class StaticService {

  title = 'Irregolari Softair';
  description = 'dashboard manager per soci e bilancio dell\'ass. Irregolari Softair';
  logo = '/assets/images/logo-irre.svg';

  constructor() { }

}
