import { Injectable } from '@angular/core';

@Injectable()
export class ProgressBarService {

  private readonly DEFAULTS = {
    MODE: 'indeterminate',
    VALUE: 0,
    VISIBLE: false,
    TOTAL: 100,
  };

  mode = this.DEFAULTS.MODE;
  value = this.DEFAULTS.VALUE;
  visible = this.DEFAULTS.VISIBLE;

  total = this.DEFAULTS.TOTAL;

  constructor() { }

  advance() {
    this.value += Math.ceil(100 / this.total);
  }

  reset() {
    this.mode = this.DEFAULTS.MODE;
    this.value = this.DEFAULTS.VALUE;
    this.visible = this.DEFAULTS.VISIBLE;
    this.total = this.DEFAULTS.TOTAL;
  }

}
