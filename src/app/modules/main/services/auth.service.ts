import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import 'rxjs/operators/switchMap';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

export function b64DecodeUnicode(str) {
  // Going backwards: from bytestream, to percent-encoding, to original string.
  return decodeURIComponent(atob(str).split('').map(function(c) {
    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));
}

export class EmailCredentials {
  email: string;
  password: string;
}

export class Roles {
  admin?: boolean;
  guest?: boolean;
}

export class User {
  uid?: string;
  email?: string;
  name?: string;
  roles?: Roles;
}

@Injectable()
export class AuthService {
  private _user: Subject<User> = new Subject<User>();
  user: User;
  roles: BehaviorSubject<Roles> = new BehaviorSubject<Roles>({});

  constructor(
    public afAuth: AngularFireAuth,
  ) {
    this.afAuth.authState.subscribe(data => {
      if (data) {
        data.getIdToken().then(token => {
          const { user_id, name, email, roles } = JSON.parse(b64DecodeUnicode(token.split('.')[1]));
          const definedRoles = Object.assign({admin: false, guest: true}, roles);
          this._user.next({
            uid: user_id,
            name, email,
            roles: definedRoles
          });
        });
      }
    });
    this._user.subscribe(user => {
      this.user = user;
      this.roles.next(user.roles);
    });
  }

  login(provider, payload = null): Promise<any> {
    const suffix = 'Login';
    if (typeof this[provider + suffix] === 'function') {
      return this[provider + suffix](payload)
        .then(credential => {
          return credential;
        });
    } else {
      throw new Error(`Undefined provider ${provider}`);
    }
  }

  logout(): Promise<any> {
    return this.afAuth.auth.signOut();
  }

  googleLogin(): Promise<any> {
    return this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }

  emailLogin(payload: EmailCredentials): Promise<any> {
    const {email, password} = payload;
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  hasRole(...role): Observable<boolean> {
    return this.roles.map(roles => {
      return role.some(r => roles[r] && roles[r] === true);
    });
  }

}
