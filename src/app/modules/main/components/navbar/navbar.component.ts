import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { StaticService } from '../../services/static.service';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { ProgressBarService } from '../../services/progress-bar.service';
import { MediaMatcher } from '@angular/cdk/layout';
import { MediaService } from '../../../shared/services/media.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  canImport: boolean;
  canViewLogs: boolean;

  @Input() media: MediaMatcher;
  @Output() toggle = new EventEmitter();

  constructor(
    public text: StaticService,
    public auth: AuthService,
    public router: Router,
    public progressBar: ProgressBarService,
    public mediaService: MediaService
  ) {
  }

  ngOnInit() {
    this.auth.hasRole('superAdmin').subscribe(bool => this.canImport = this.canViewLogs = bool);
  }

  logout() {
    this.auth.logout()
      .then(res => this.router.navigateByUrl('login'));
  }

}
