import { AfterViewInit, Component, OnInit } from '@angular/core';
import { StaticService } from '../../services/static.service';
import { AuthService, EmailCredentials } from '../../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {
  loginForm: FormGroup;
  visible = false;
  open = false;
  email = false;

  constructor(
    public text: StaticService,
    public auth: AuthService,
    public router: Router,
    public fb: FormBuilder,
  ) {
    auth.afAuth.authState.subscribe(user => {
      if (user) {
        this.router.navigateByUrl('');
      }
    });
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  ngAfterViewInit() {
    setTimeout(() => this.visible = true, 0);
    setTimeout(() => {
      this.open = true;
    }, 2000);
  }

  login(provider, payload = null) {
    this.auth.login(provider, payload)
      .then(res => {
        this.router.navigateByUrl('');
      })
      .catch(err => {
        if (err.code === 'auth/user-not-found') {
          this.loginForm.controls.email.setErrors({notFound: true});
        }
      });
  }

  emailLogin() {
    if (this.loginForm.valid) {
      const {email, password} = this.loginForm.value;
      this.login('email', <EmailCredentials>{email, password});
    }
  }

  openEmailForm() {
    this.email = true;
  }

  getEmailErrorMessage() {
    return this.loginForm.controls.email.hasError('required') ? 'Email necessaria' :
      this.loginForm.controls.email.hasError('email') ? 'Non è una email valida' :
      this.loginForm.controls.email.hasError('notFound') ? 'Utente non trovato' : '';
  }

}
