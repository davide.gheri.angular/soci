import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { ActivatedRoute, NavigationEnd, Router, RouterOutlet, UrlSegment } from '@angular/router';
import { OverlayService } from '../../../shared/services/overlay.service';
import { routerTransition } from '../../router.animations';
import { Subscription } from 'rxjs/Subscription';
import { MediaMatcher } from '@angular/cdk/layout';

@Component({
  selector: 'app-main',
  animations: [routerTransition],
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('infosnav') infosnav;
  @ViewChild('snavbar') snavbar;

  opened = false;
  infoOpened = false;
  overlay: boolean;
  canEditUsers = false;

  createLink: string;
  links = {
    soci: '/soci/create',
    bilancio: '/bilancio/create'
  };

  routeChild: any;
  routeChildSub: Subscription;

  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;

  constructor(
    public auth: AuthService,
    public router: Router,
    public route: ActivatedRoute,
    public overlayService: OverlayService,
    private changeDetectorRef: ChangeDetectorRef,
    public media: MediaMatcher,
  ) {
    this.overlayService.visible.subscribe(bool => this.overlay = bool);

    this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd && this.routeChild !== this.route.firstChild) {
        if (this.routeChildSub) { this.routeChildSub.unsubscribe(); }
        this.routeChild = this.route.firstChild;
        this.routeChildSub = this.route.firstChild.url.subscribe((u: UrlSegment[]) => {
          if (u[0] && u[0].path !== '') {
            this.createLink = this.links[u[0].path] || null;
          }
        });
      }
    });

    this.mobileQuery = media.matchMedia('(max-width: 1100px)');
    this._mobileQueryListener = () => {
      changeDetectorRef.detectChanges();
      if (this.mobileQuery.matches) {
        this.opened = this.infoOpened = false;
      } else {
        this.opened = this.infoOpened = true;
      }
    };
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    this.auth.hasRole('admin').subscribe(bool => this.canEditUsers = bool);
  }


  ngAfterViewInit() {
    setTimeout(() => {
      if (!this.mobileQuery.matches) {
        this.opened = this.infoOpened = true;
      } else {
        this.opened = this.infoOpened = false;
      }
    }, 0);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  toggle($event) {
    // this.snav.toggle();
  }

  infoToggle() {
    this.infoOpened = !this.infoOpened;
  }

  getState(outlet: RouterOutlet) {
    return outlet.activatedRouteData.animation;
  }

}
