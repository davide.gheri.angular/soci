
export * from './soci/soci.component';
export * from './soci-list/soci-list.component';
export * from './socio/socio.component';
export * from './socio-show/socio-show.component';
export * from './socio-address/socio-address.component';
export * from './file-upload-dialog/file-upload-dialog.component';
export * from './mail-dialog/mail-dialog.component';
