import { Component, Inject, OnInit } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase } from 'angularfire2/database';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Socio } from '../../models';
import { SociService } from '../../services';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-file-upload-dialog',
  templateUrl: './file-upload-dialog.component.html',
  styleUrls: ['./file-upload-dialog.component.scss']
})
export class FileUploadDialogComponent implements OnInit {

  task: AngularFireUploadTask;
  percentage: Observable<number>;
  snapshot: Observable<any>;
  downloadUrl: Observable<string>;
  isHovering: boolean;

  socio: Socio;

  constructor(
    private storage: AngularFireStorage,
    private db: AngularFireDatabase,
    private sociService: SociService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogref: MatDialogRef<FileUploadDialogComponent>
  ) {
    this.socio = this.data.socio;
    this.socio.certificato_file = this.socio.certificato_file || [];
  }

  ngOnInit() {
  }

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  startUpload(event: FileList) {
    const file = event.item(0);
    const path = `${this.socio.id}/${file.name}`;
    this.task = this.storage.upload(path, file);
    this.percentage = this.task.percentageChanges();
    this.snapshot = this.task.snapshotChanges().pipe(
      tap(snap => {
        if (snap.bytesTransferred === snap.totalBytes) {
        }
      })
    );

    this.downloadUrl = this.task.downloadURL();
    this.downloadUrl.subscribe(url => {
      this.socio.certificato_file.push({url: url, name: file.name, path});
      this.sociService.updateSocio({certificato_file: this.socio.certificato_file});
    });
  }

  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes;
  }

  deleteFile($event, index) {
    $event.preventDefault();
    $event.stopPropagation();
    const ref = this.storage.ref(this.socio.certificato_file[index].path);
    this.socio.certificato_file = this.socio.certificato_file.filter((o, i) => i !== index);
    this.sociService.updateSocio({certificato_file: this.socio.certificato_file})
      .then(res => {
        ref.delete();
      });
  }

  closeDialog() {
    this.dialogref.close();
  }

}
