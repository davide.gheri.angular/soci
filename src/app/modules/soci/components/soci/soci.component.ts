import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, Sort } from '@angular/material';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { SociDataSource } from '../../soci-data-source';
import { AuthService } from '../../../main/services/auth.service';
import { Insurance, Socio } from '../../models';
import { InsurancesService, SociService } from '../../services';
import * as moment from 'moment';
import { MediaService } from '../../../shared/services/media.service';

@Component({
  selector: 'app-soci',
  templateUrl: './soci.component.html',
  styleUrls: ['./soci.component.scss']
})
export class SociComponent implements OnInit {

  columns = ['nickname', 'nome', 'cognome', 'assicurazione', 'scadenza_certificato'];
  dataSource: SociDataSource;
  canLink: boolean;
  isCollapsed = true;

  filters = {
    search: '',
    assicurazione: 'ALL',
    certificato: 'ALL'
  };

  filter$: BehaviorSubject<{}>;
  sort$: BehaviorSubject<Sort>;
  soci$: Observable<Socio[]>;
  soci: Observable<Socio[]>;
  insurances: Observable<Insurance[]>;

  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public sociService: SociService,
    private insurancesService: InsurancesService,
    private auth: AuthService,
    private router: Router,
    public mediaService: MediaService,
  ) {
    this.auth.hasRole('admin').subscribe(bool => this.canLink = bool);
    this.insurances = this.insurancesService.insurances;
  }

  ngOnInit() {
    this.filter$ = new BehaviorSubject<{}>(this.filters);
    this.sort$ = new BehaviorSubject<Sort>({active: 'nickname', direction: 'desc'});
    this.soci$ = this.sociService.soci;
    this.soci = combineLatest(this.soci$, this.filter$, this.sort$,
      (soci: Socio[], filter: {search: string, assicurazione: string, certificato: string}, sort: Sort) => {
      let filtered = soci;
      if (filter) {
        filtered = soci.filter((socio: Socio) => {
          let search, assicurazione, certificato;
          search = assicurazione = certificato = true;
          if (filter.search && filter.search !== '') {
            const lowerCase = filter.search.trim().toLowerCase();
            search = socio.nome.toLowerCase().indexOf(lowerCase) >= 0
              || socio.cognome.toLowerCase().indexOf(lowerCase) >= 0
              || socio.nickname.toLowerCase().indexOf(lowerCase) >= 0;
          }
          if (filter.assicurazione && filter.assicurazione !== 'ALL') {
            assicurazione = socio.assicurazione.toLowerCase() === filter.assicurazione.trim().toLowerCase();
          }
          if (filter.certificato && filter.certificato !== 'ALL') {
            const now = moment();
            const data = moment(socio.scadenza_certificato, 'DD-MM-YYYY');
            if (filter.certificato === 'VALID') {
              certificato = moment(data).isAfter(now) && data.isValid();
            } else {
              certificato = moment(now).isAfter(data) || !data.isValid();
            }
          }
          return search && assicurazione && certificato;
        });
      }
      if (sort) {
        filtered.sort((a, b) => {
          if (a[sort.active].toLowerCase() < b[sort.active].toLowerCase()) {
            return sort.direction === 'desc' ? -1 : 1;
          }
          if (a[sort.active].toLowerCase() > b[sort.active].toLowerCase()) {
            return sort.direction === 'desc' ? 1 : -1;
          }
          return 0;
        });
      }
      return filtered;
    });
    this.dataSource = new SociDataSource(this.soci);

    this.sort.sortChange.subscribe((sort: Sort) => {
      this.sort$.next(sort);
    });

  }

  filterSocio(obj) {
    this.filters = Object.assign(this.filters, obj);
    this.filter$.next(this.filters);
  }

  onRowClick(socio) {
    if (this.canLink) {
      this.router.navigate([`soci/${socio.id}`]);
    }
  }


}
