import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Socio } from '../../models';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SociService } from '../../services';

@Component({
  selector: 'app-mail-dialog',
  templateUrl: './mail-dialog.component.html',
  styleUrls: ['./mail-dialog.component.scss']
})
export class MailDialogComponent implements OnInit {

  socio: Socio;
  mailForm: FormGroup;
  errors: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogref: MatDialogRef<MailDialogComponent>,
    private fb: FormBuilder,
    private sociService: SociService,
  ) {
    this.socio = data.socio;
  }

  ngOnInit() {
    this.mailForm = this.fb.group({
      title: '',
      message: ''
    });
  }

  send() {
    if (this.mailForm.valid && this.socio.email) {
      this.mailForm.disable();
      this.sociService.sendEmail(this.mailForm.value, this.socio.email)
        .then(res => {
          console.log(res)
          this.mailForm.enable();
          this.dialogref.close(true);
          this.errors = false;
        })
        .catch(e => {
          console.log('ERROR')
          console.log(e)
          this.mailForm.enable();
          this.errors = true;
        });
    }
  }

  cancel() {
    this.dialogref.close(false);
  }

}
