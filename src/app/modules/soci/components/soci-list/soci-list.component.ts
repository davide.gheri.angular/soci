import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { SociService } from '../../services';
import { Socio } from '../../models';

@Component({
  selector: 'app-soci-list',
  templateUrl: './soci-list.component.html',
  styleUrls: ['./soci-list.component.scss']
})
export class SociListComponent implements OnInit {

  soci: Observable<Socio[]>

  constructor(
    public sociService: SociService
  ) {
    this.soci = this.sociService.soci;
  }

  ngOnInit() {
  }

}
