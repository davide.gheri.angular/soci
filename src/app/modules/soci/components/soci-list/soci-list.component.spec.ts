import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SociListComponent } from './soci-list.component';

describe('SociListComponent', () => {
  let component: SociListComponent;
  let fixture: ComponentFixture<SociListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SociListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SociListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
