import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocioAddressComponent } from './socio-address.component';

describe('SocioAddressComponent', () => {
  let component: SocioAddressComponent;
  let fixture: ComponentFixture<SocioAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocioAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocioAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
