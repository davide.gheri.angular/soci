import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { SociService } from '../../services';
import { Address, Socio } from '../../models';

@Component({
  selector: 'app-socio-address',
  templateUrl: './socio-address.component.html',
  styleUrls: ['./socio-address.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SocioAddressComponent implements OnInit, OnDestroy {

  socioId: string;
  socio: Observable<Socio | {}>;
  addressForm: FormGroup;

  private _addressSub: Subscription;

  constructor(
    private sociService: SociService,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.socio = this.sociService.socio;
    this.buildForm();
  }

  buildForm(): void {
    this.addressForm = this.fb.group({
      indirizzo: [''],
      civico: [''],
      cap: [''],
      comune: [''],
      provincia: ['']
    });

    this._addressSub = this.socio.subscribe((socio: Socio) => {
      this.addressForm.reset();
      if (socio.address) {
        this.addressForm.patchValue(socio.address);
      }
      this.socioId = socio.id;
    });
  }

  saveAddressChanges() {
    if (this.addressForm.valid) {
      const data = this.addressForm.value;
      this.sociService.updateAddress(data);
    } else {
      Object.keys(this.addressForm.controls).forEach(ctrl => {
        this.addressForm.get(ctrl).markAsTouched();
        this.addressForm.get(ctrl).updateValueAndValidity();
      });
    }
  }

  ngOnDestroy(): void {
    this._addressSub.unsubscribe();
  }


}
