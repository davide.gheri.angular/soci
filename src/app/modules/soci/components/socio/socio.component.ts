import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterOutlet } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { PageTitleService } from '../../../shared/services/page-title.service';
import { MatDialog } from '@angular/material';
import { DialogConfirmComponent } from '../../../shared/components/dialog-confirm/dialog-confirm.component';
import { routerTransition } from '../../router.animations';
import { SociService } from '../../services';
import { Socio } from '../../models';
import { FileUploadDialogComponent } from '../file-upload-dialog/file-upload-dialog.component';
import { MailDialogComponent } from '../mail-dialog/mail-dialog.component';
import { MediaService } from '../../../shared/services/media.service';

@Component({
  selector: 'app-socio',
  animations: [routerTransition],
  templateUrl: './socio.component.html',
  styleUrls: ['./socio.component.scss']
})
export class SocioComponent implements OnInit, OnDestroy {

  socio: Socio;
  isNew = false;

  private _socioSub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private sociService: SociService,
    private pageTitleService: PageTitleService,
    public dialog: MatDialog,
    public mediaService: MediaService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(value => {
      const id = value.id;
      if (id !== 'create') {
        this.isNew = false;
        this.sociService.getSocio(id);
      } else {
        this.isNew = true;
        this.sociService.socio.next(new Socio);
      }
    });
    this._socioSub = this.sociService.socio
      .subscribe((socio: Socio) => {
        this.socio = socio;
        this.pageTitleService.replaceSection(socio.id, socio.nickname);
      });
  }

  openConfirmDialog() {
    this.dialog.open(DialogConfirmComponent, {
      data: {
        title: 'Vuoi davvero eliminare il socio {nickname}?',
        item: this.socio
      }
    }).afterClosed().subscribe(bool =>
      (bool && this.sociService.deleteSocio(this.socio)
        .then(res => this.router.navigate(['/soci'])))
    );
  }

  openFileDialog() {
    this.dialog.open(FileUploadDialogComponent, {
      data: {socio: this.socio}
    });
  }

  openMailDialog() {
    this.dialog.open(MailDialogComponent, {
      data: {socio: this.socio}
    });
  }

  ngOnDestroy() {
    this._socioSub.unsubscribe();
  }

  getState(outlet: RouterOutlet) {
    return outlet.activatedRouteData.animation;
  }

}
