import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocioShowComponent } from './socio-show.component';

describe('SocioShowComponent', () => {
  let component: SocioShowComponent;
  let fixture: ComponentFixture<SocioShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocioShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocioShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
