import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { Subscription } from 'rxjs/Subscription';
import { InsurancesService, NationsService, SociService } from '../../services';
import { Nation, Insurance, Socio, SOCIO_DATES, SOCIO_DOCUMENT_TYPES } from '../../models';
import * as moment from 'moment';
import { MatDialog } from '@angular/material';
import { MediaService } from '../../../shared/services/media.service';

@Component({
  selector: 'app-socio-show',
  templateUrl: './socio-show.component.html',
  styleUrls: ['./socio-show.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SocioShowComponent implements OnInit, OnDestroy {
  document_types = SOCIO_DOCUMENT_TYPES;

  $nations: Observable<Nation[]>;
  $nationFilter: any;
  nations: any;

  socio: Observable<Socio | {}>;
  insurances: Observable<Insurance[]>;
  socioForm: FormGroup;

  private _socioSub: Subscription;

  step = 0;
  isNew: boolean;

  constructor(
    private sociService: SociService,
    private insurancesService: InsurancesService,
    private nationsService: NationsService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    public mediaService: MediaService,
  ) {
    this.route.params.subscribe(params => {
      if (params.id === 'create') {
        this.isNew = true;
      } else {
        // this.sociService.getSocio(params.id);
        this.isNew = false;
      }
    });
  }

  ngOnInit() {
    this.socio = this.sociService.socio;
    this.insurances = this.insurancesService.insurances;
    this.buildForm();

    // Nations autocomplete filters
    this.$nations = this.nationsService.countries;
    this.$nationFilter = this.socioForm.controls['nazionalita'].valueChanges;
    this.nations = combineLatest(this.$nations, this.$nationFilter, (nations: Nation[], filter: string) => {
      return nations.filter((nation: Nation) => {
        if (filter && filter !== '') {
          return nation.name.toLowerCase().indexOf(filter.toLowerCase())  >= 0;
        }
        return true;
      });
    });
  }

  buildForm() {
    this.socioForm = this.fb.group({
      nome: ['', Validators.required],
      cognome: ['', Validators.required],
      nickname: [''],
      assicurazione: [''],
      codice_fiscale: ['', Validators.required],
      data_nascita: ['', Validators.required],
      email: [''],
      luogo_nascita: ['', Validators.required],
      nazionalita: ['', Validators.required],
      numero_documento: ['', Validators.required],
      tipo_documento: ['', Validators.required],
      scadenza_certificato: [''],
      telefono: [''],
      tessera: [''],
      quota: [''],
    });

    this._socioSub = this.socio.subscribe(socio => {
      this.socioForm.reset();
      this.socioForm.patchValue(Object.keys(socio).reduce((obj, field) => {
        if (SOCIO_DATES.includes(field)) {
          obj[field] = moment(socio[field], 'DD-MM-YYYY');
        }
        return obj;
      }, socio));
    });
  }

  saveSocioChanges() {
    if (this.socioForm.valid) {
      const data = Object.keys(this.socioForm.value).reduce((obj, field) => {
        if (obj[field] && obj[field]._isAMomentObject) {
          obj[field] = obj[field].format('DD/MM/YYYY');
        }
        return obj;
      }, this.socioForm.value);

      if (this.isNew) {
        this.sociService.addSocio(data as Socio)
          .then(id => this.router.navigate(['/soci/' + id]));
      } else {
        this.sociService.updateSocio(data);
      }
    } else {
      Object.keys(this.socioForm.controls).forEach(ctrl => {
        this.socioForm.get(ctrl).markAsTouched();
        this.socioForm.get(ctrl).updateValueAndValidity();
      });
    }
  }

  nextStep() {
    if (!this.isNew) {
      this.step++;
    }
  }

  prevStep() {
    this.step--;
  }

  setStep(n) {
    this.step = n;
  }

  ngOnDestroy() {
    this._socioSub.unsubscribe();
  }

}
