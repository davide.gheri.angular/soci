import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  SociComponent,
  SociListComponent,
  SocioComponent,
  SocioAddressComponent,
  SocioShowComponent,
  MailDialogComponent,
} from './components';
import { MaterialModule } from '../material.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { InsurancesService, NationsService, SociService } from './services';
import { ActivityLogModule } from '../activity-log/activity-log.module';
import { DropZoneDirective } from './directives/drop-zone.directive';
import { FileUploadDialogComponent } from './components';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ActivityLogModule,
    HttpClientModule,
  ],
  declarations: [
    SociComponent,
    SociListComponent,
    SocioComponent,
    SocioShowComponent,
    SocioAddressComponent,
    DropZoneDirective,
    FileUploadDialogComponent,
    MailDialogComponent,
  ],
  exports: [
    SociComponent,
    SociListComponent,
    SocioComponent,
    DropZoneDirective,
  ],
  providers: [SociService, InsurancesService, NationsService],
  entryComponents: [
    FileUploadDialogComponent,
    MailDialogComponent,
  ]
})
export class SociModule { }
