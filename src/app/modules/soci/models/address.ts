
export class Address {
  id?: string;
  indirizzo?: string;
  civico?: string | number;
  cap?: string | number;
  comune?: string;
  provincia?: string;
}