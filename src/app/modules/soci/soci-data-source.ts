import { DataSource } from '@angular/cdk/collections';
import { Socio } from './models/socio';
import { CollectionViewer } from '@angular/cdk/typings/collections';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


export class SociDataSource implements DataSource<Socio> {

  constructor(private _data: any) {}

  connect(collectionViewer: CollectionViewer): Observable<any[]> {
    return this._data;
  }

  disconnect(collectionViewer: CollectionViewer): void {

  }
}
