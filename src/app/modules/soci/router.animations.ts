import { animate, query, style, transition, trigger } from '@angular/animations';

export const routerTransition = trigger('routerTransition', [
  transition('form => address', [
    query(':enter', style({
      position: 'absolute',
      width: '100%',
      transform: 'translateX(-120%)'
    }), {optional: true}),
    query(':leave', animate('200ms ease', style({
      position: 'absolute',
      width: '100%',
      transform: 'translateX(100%)'
    })), {optional: true}),
    query(':enter', animate('200ms ease', style({
      opacity: 1,
      transform: 'translateX(0%)'
    })), {optional: true})
  ]),
  transition('address => form', [
    query(':enter', style({
      position: 'absolute',
      width: '100%',
      transform: 'translateX(100%)'
    }), {optional: true}),
    query(':leave', animate('200ms ease', style({
      position: 'absolute',
      width: '100%',
      transform: 'translateX(-120%)'
    })), {optional: true}),
    query(':enter', animate('200ms ease', style({
      opacity: 1,
      transform: 'translateX(0%)'
    })), {optional: true})
  ])
]);
