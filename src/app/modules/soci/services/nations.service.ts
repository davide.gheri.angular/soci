import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase } from 'angularfire2/database';
import { Nation } from '../models';

@Injectable()
export class NationsService {
  countries: Observable<Nation[]>;

  constructor(
    public db: AngularFireDatabase,
  ) {
    this.countries = this.db.list<Nation>('countries').valueChanges();
  }

}
