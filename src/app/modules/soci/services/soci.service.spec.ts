import { TestBed, inject } from '@angular/core/testing';

import { SociService } from './soci.service';

describe('SociService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SociService]
    });
  });

  it('should be created', inject([SociService], (service: SociService) => {
    expect(service).toBeTruthy();
  }));
});
