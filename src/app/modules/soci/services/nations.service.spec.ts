import { TestBed, inject } from '@angular/core/testing';

import { NationsService } from './nations.service';

describe('NationsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NationsService]
    });
  });

  it('should be created', inject([NationsService], (service: NationsService) => {
    expect(service).toBeTruthy();
  }));
});
