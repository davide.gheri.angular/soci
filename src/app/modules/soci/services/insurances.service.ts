import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase, SnapshotAction } from 'angularfire2/database';
import { CommonService } from '../../shared/services/common.service';
import { Insurance } from '../models';

@Injectable()
export class InsurancesService extends CommonService {
  insurances: Observable<Insurance[]>;

  constructor(
    public db: AngularFireDatabase,
  ) {
    super();
    this.insurances = this.db.list<Insurance>('insurances').snapshotChanges()
      .map((actions: SnapshotAction[]) => {
        return actions.map((action: SnapshotAction) => this.fromActionToModel<Insurance>(action));
      });
  }

}
