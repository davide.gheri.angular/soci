import { Injectable } from '@angular/core';
import { Socio } from '../models';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ProgressBarService } from '../../main/services/progress-bar.service';
import { OverlayService } from '../../shared/services/overlay.service';
import { AngularFireDatabase, AngularFireObject, SnapshotAction } from 'angularfire2/database';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { CommonService } from '../../shared/services/common.service';
import { ActivitiesService } from '../../activity-log/services/activities.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class SociService extends CommonService {
  soci: Observable<Socio[]>;
  private socioRef: AngularFireObject<Socio>;
  socio: BehaviorSubject<Socio | {}> = new BehaviorSubject<Socio | {}>({});

  constructor(
    public db: AngularFireDatabase,
    public progressBar: ProgressBarService,
    public overlayService: OverlayService,
    public activityLog: ActivitiesService,
    public http: HttpClient,
  ) {
    super();
    this.soci = this.db.list<Socio>('soci', (ref) => ref.orderByChild('nickname')).snapshotChanges()
      .map((actions: SnapshotAction[]) => {
        return actions.map((action: SnapshotAction) => this.fromActionToModel<Socio>(action)).sort((a, b) => {
          return a.nickname.toLowerCase() < b.nickname.toLowerCase() ? -1 : 1;
        });
      });
  }

  getSocio(id) {
    this.socioRef = this.db.object('soci/' + id);
    this.socioRef.snapshotChanges()
      .map((action: SnapshotAction) => this.fromActionToModel<Socio>(action))
      .subscribe((socio: Socio) => {
        this.socio.next(socio);
      });
  }

  updateSocio(data, id?): Promise<any> {
    let ref = this.socioRef as any;
    if (id) {
      ref = this.db.object('soci/' + id) as any;
    }
    this.progressBar.visible = true;
    return ref.update(data)
      .then(res => {
        this.activityLog.log(data, 'update', 'soci', ref.query.key);
        this.progressBar.reset();
        return res;
      });
  }

  addSocio(data): any {
    this.progressBar.visible = true;
    return this.db.list('soci').push(data)
      .then(res => {
        this.activityLog.log(data, 'create', 'soci', res.key);
        this.progressBar.reset();
        return res.key;
      });
  }

  deleteSocio(data, id?): Promise<any> {
    let ref = this.socioRef as any;
    if (id) {
      ref = this.db.object('soci/' + id) as any;
    }
    this.progressBar.visible = true;
    this.overlayService.visible.next(true);
    return ref.remove()
      .then(res => {
        this.activityLog.log(Object.keys(data).reduce((obj, field) => {
          if (obj[field]._isAMomentObject) {
            obj[field] = obj[field].format('DD/MM/YYYY');
          }
          return obj;
        }, data), 'delete', 'soci', ref.query.key);
        this.progressBar.reset();
        this.overlayService.visible.next(false);
        return res;
      });
  }

  updateAddress(data, id?): Promise<any> {
    return this.updateSocio({address: data}, id);
  }

  sendEmail(payload: {title: string, message: string}, email: string) {
    return this.http.post(this.functions.mail, {email, payload}).toPromise();
  }

}
