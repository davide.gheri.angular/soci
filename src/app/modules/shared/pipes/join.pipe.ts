import { Pipe, PipeTransform } from '@angular/core';
import { isArray } from 'util';

@Pipe({
  name: 'join'
})
export class JoinPipe implements PipeTransform {

  transform(value: any, delimeter: string = ', '): any {
    return !isArray(value) ? value : value.join(delimeter);
  }

}
