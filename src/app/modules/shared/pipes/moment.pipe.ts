import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'moment'
})
export class MomentPipe implements PipeTransform {

  transform(value: string, fromFormat?, format = 'DD/MM/YYYY'): string | void {
    const m = moment(value, fromFormat);
    if (m.isValid()) {
      return m.format(format);
    }
    return '';
  }

}
