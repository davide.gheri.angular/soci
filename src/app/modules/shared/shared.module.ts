import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JoinPipe } from './pipes/join.pipe';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { PageHeaderComponent } from './components/page-header/page-header.component';
import { PageTitleService } from './services/page-title.service';
import { LogPipe } from './pipes/log.pipe';
import { ValuesPipe } from './pipes/values.pipe';
import { DialogConfirmComponent } from './components/dialog-confirm/dialog-confirm.component';
import { MaterialModule } from '../material.module';
import { OverlayService } from './services/overlay.service';
import { QuickButtonsComponent } from './components/quick-buttons/quick-buttons.component';
import { MomentPipe } from './pipes/moment.pipe';
import { MediaService } from './services/media.service';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
  ],
  declarations: [
    CapitalizePipe,
    JoinPipe,
    LogPipe,
    ValuesPipe,
    PageHeaderComponent,
    DialogConfirmComponent,
    QuickButtonsComponent,
    MomentPipe,
  ],
  exports: [
    CapitalizePipe,
    JoinPipe,
    LogPipe,
    ValuesPipe,
    MomentPipe,
    PageHeaderComponent,
    DialogConfirmComponent,
  ],
  providers: [
    PageTitleService,
    OverlayService,
    MediaService
  ],
  entryComponents: [
    DialogConfirmComponent
  ],
})
export class SharedModule { }
