import { Component, OnInit } from '@angular/core';
import { PageTitleService } from '../../services/page-title.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent implements OnInit {

  title: any;

  constructor(
    private pageTitleService: PageTitleService,
    public location: Location
  ) {
    this.title = pageTitleService.title;
  }

  ngOnInit(): void {
  }

}
