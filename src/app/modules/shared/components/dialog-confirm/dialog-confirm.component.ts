import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-dialog-confirm',
  templateUrl: './dialog-confirm.component.html',
  styleUrls: ['./dialog-confirm.component.scss']
})
export class DialogConfirmComponent implements OnInit {

  private readonly REGEX = new RegExp(/\{(.)*\}/g);

  title: string;
  item: any;
  confirm: string;
  cancel: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogref: MatDialogRef<DialogConfirmComponent>
  ) { }

  ngOnInit() {
    this.item = this.data.item;
    this.confirm = this.data.confirm || 'Conferma';
    this.cancel = this.data.cancel || 'Annulla';
    this.setTitle();
  }

  setTitle(): void {
    const _title = this.data.title || 'Conferma';
    let replace = '';
    const match = _title.match(this.REGEX) ? _title.match(this.REGEX)[0] : null;
    if (match) {
      const field = match.slice(1, -1);
      replace = this.item ? this.item[field] || '' : '';
    }
    this.title = _title.replace(match, replace);
  }

  close(bool) {
    this.dialogref.close(bool);
  }

}
