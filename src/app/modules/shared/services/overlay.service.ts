import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class OverlayService {

  visible: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() { }

}
