import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class PageTitleService {
  _title: any;

  title: BehaviorSubject<any> = new BehaviorSubject<any>({'base': 'Soci'});

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) {
    const urlPathObj = this.route.snapshot.firstChild.firstChild.url.reduce((obj, url) => {
      obj[url.path] = url.path;
      return obj;
    }, {});
    this._title = urlPathObj;
    this.title.next(urlPathObj);
    this.router.events
      .filter(e => e instanceof NavigationEnd)
      .switchMap(e => (this.route.firstChild.firstChild && this.route.firstChild.firstChild.url) || Observable.of([]))
      .subscribe(urls => {
        const newUrlPathObj = urls.reduce((obj, url) => {
          obj[url.path] = url.path;
          return obj;
        }, {});
        this._title = newUrlPathObj;
        this.title.next(newUrlPathObj);
      });
  }

  replaceSection(search, replace): any {
    if (this._title[search]) {
      this._title[search] = replace;
      this.title.next(Object.assign({}, this._title));
    }
  }

}
