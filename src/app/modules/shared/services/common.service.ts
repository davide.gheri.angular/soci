import { SnapshotAction } from 'angularfire2/database';
import { environment } from '../../../../environments/environment';

export abstract class CommonService {

  public functions: any;

  constructor() {
    this.functions = Object.keys(environment.functions).reduce((obj, name) => {
      obj[name] = environment.functionsUrl + environment.functions[name];
      return obj;
    }, {});
  }

  protected fromActionToModel<T>(action: SnapshotAction) {
    const id = action.key;
    const data = action.payload.val();
    return {id, ...data} as T;
  }
}
