import { Socio } from '../../soci/models/socio';

const fillable = [
  'nome', 'cognome', 'nickname',
  'luogo_nascita', 'data_nascita',
  'codice_fiscale', 'tipo_documento',
  'numero_documento', 'telefono', 'email',
  'assicurazione', 'quota', 'scadenza_certificato',
  'tessera'
];

export class ImportSocio extends Socio {

}


export class JsonImportSocio extends Socio {
  indirizzo?: string;
  civico?: string;
  cap?: string | number;
  comune?: string;
  provincia?: string;
}
