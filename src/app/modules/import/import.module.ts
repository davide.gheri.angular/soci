import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportComponent } from './components/import/import.component';
import { MaterialModule } from '../material.module';
import { ImportService } from './services/import.service';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
  ],
  declarations: [
    ImportComponent,
  ],
  exports: [
    ImportComponent,
  ],
  providers: [ImportService]
})
export class ImportModule { }
