import { Injectable } from '@angular/core';
import { ImportSocio, JsonImportSocio } from '../models/import-socio';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Socio } from '../../soci/models/socio';
import { ProgressBarService } from '../../main/services/progress-bar.service';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

@Injectable()
export class ImportService {
  readonly list = 'soci';
  reader: FileReader;

  private sociList: AngularFireList<Socio>;

  constructor(
    private afs: AngularFirestore,
    private db: AngularFireDatabase,
    private progressBar: ProgressBarService,
  ) {
    this.reader = new FileReader();
    this.sociList = this.db.list(this.list);
  }

  import(file: File): Promise<any> {
    return new Promise((resolve, reject) => {
      this.reader.onload = () => {
        this.parseJson(this.reader.result)
          .catch(err => reject(err))
          .then(soci => {
            this.progressBar.total = soci.length;
            this.progressBar.mode = 'determinate';
            this.progressBar.visible = true;
            this.upload(soci)
              .then(res => {
                this.progressBar.reset();
                resolve(res);
              })
              .catch(err => reject(err));
          });
      };
      this.reader.readAsText(file);
    });
  }

  private parseJson(string): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        const address_fields = ['indirizzo', 'civico', 'cap', 'comune', 'provincia'];
        let parsed = JSON.parse(string);
        if (!(parsed instanceof Array)) {
          parsed = Object.keys(parsed).reduce((array, k) => {
            array.push(parsed[k]);
            return array;
          }, []);
        }
        const obj: JsonImportSocio[] = parsed.map((socio: JsonImportSocio) => {
          let formatted = socio;
          if (!socio.hasOwnProperty('address')) {
            formatted = Object.keys(socio).reduce((obj, field) => {
              if (address_fields.includes(field)) {
                obj.address[field] = socio[field];
              } else {
                obj[field] = socio[field];
              }
              return obj;
            }, {address: {}});
          }
          return formatted;
        });
        resolve(obj);
      } catch (error) {
        reject(error);
      }
    });
  }

  upload(soci): Promise<any> {
    return Promise.all(soci.map(rawSocio => this.addSocio(rawSocio)));
  }

  addSocio(rawSocio) {
    return this.sociList.push(rawSocio);
  }

}
