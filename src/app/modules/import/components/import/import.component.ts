import { Component, OnInit } from '@angular/core';
import { ImportService } from '../../services/import.service';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.scss']
})
export class ImportComponent implements OnInit {

  readonly JSON_FILETYPE = 'application/json';
  errors: string;
  importText: string;

  constructor(
    private importService: ImportService,
  ) { }

  ngOnInit() {
  }

  import($event) {
    const file: File = $event.target.files[0];
    if (!file || file.type !== this.JSON_FILETYPE) {
      this.errors = 'Non un valido json';
    } else {
      this.importText = 'Import in corso...';
      this.importService.import(file)
        .then(res => {
          console.log(res)
          this.importText = '';
        })
        .catch(err => {
          console.log(err);
          this.errors = 'Errore nell\'import, riprovare';
        });
    }
  }

}
