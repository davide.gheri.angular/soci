import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { MaterialModule, FirebaseModule } from './modules';
import { MainModule } from './modules/main/main.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouteModule } from './modules/route.module';
import { SociModule } from './modules/soci/soci.module';
import { InfoModule } from './modules/info/info.module';
import { ImportModule } from './modules/import/import.module';
import { BilancioModule } from './modules/bilancio/bilancio.module';
import { ActivityLogModule } from './modules/activity-log/activity-log.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    RouteModule,
    AngularFireModule.initializeApp(environment.firebase),
    FirebaseModule,
    MaterialModule,
    MainModule,
    SociModule,
    InfoModule,
    ImportModule,
    BilancioModule,
    ActivityLogModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
