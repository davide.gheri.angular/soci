import { trigger, animate, style, group, animateChild, query, stagger, transition, state } from '@angular/animations';

export const _routerTransition = trigger('_routerTransition', [
  transition(':enter', [
    style({opacity: 0}),
    animate('.5s ease-in-out', style({opacity: 1}))
  ]),
  transition(':leave', [
    style({opacity: 1}),
    animate('.2s ease-in-out', style({opacity: 0}))
  ]),
]);

